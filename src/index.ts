import {
  createStrel, crossSectionsToGreyscale,
  getValuesFromMatrix, greyscaleCrossSections,
  GreyscaleFormula,
  greyscaleToBlackAndWhite,
  greyscaleToBlackWhiteAndRed,
  imageToMatrix,
  invert,
  Output,
  RGB,
  rgbaToGreyscale,
  Strel,
  StructuringElementType
} from './functions';
import {MatrixCanvas} from './image-matrix';
import {makeFileInput} from './make-file-input';
import {CheckboxControl, FormHandler, NumberControl, RadioControl, TextControl} from './form-handler';
import {
  closing,
  diff,
  dilation,
  dilationAnimation,
  erosion,
  erosionAnimation, greyscaleClosing, greyscaleDilation, greyscaleErosion, greyscaleOpening, hitOrMiss,
  opening,
  rgbDiff
} from "./morphological-functions";
import {CanvasActions} from "./canvas-actions";
import {startWith} from "rxjs/operators";

const canvas = document.querySelector('#canvas') as HTMLCanvasElement;
const canvas2 = document.querySelector('#canvas-2') as HTMLCanvasElement;
const erosionBtn = document.querySelector('#erosion-btn') as HTMLButtonElement;
const dilationBtn = document.querySelector('#dilation-btn') as HTMLButtonElement;
const openingBtn = document.querySelector('#opening-btn') as HTMLButtonElement;
const closingBtn = document.querySelector('#closing-btn') as HTMLButtonElement;
const tophatBtn = document.querySelector('#tophat-btn') as HTMLButtonElement;
const hitOrMissBtn = document.querySelector('#hit-or-miss-btn') as HTMLButtonElement;
const hitOrMissOpeningBtn = document.querySelector('#hit-or-miss-opening-btn') as HTMLButtonElement;
const gradientBtn = document.querySelector('#gradient-btn') as HTMLButtonElement;
const diffBtn = document.querySelector('#diff-btn') as HTMLButtonElement;
const rgbDiffBtn = document.querySelector('#rgb-diff-btn') as HTMLButtonElement;
const strelBtn = document.querySelector('#strel-btn') as HTMLButtonElement;
const strel2Btn = document.querySelector('#strel2-btn') as HTMLButtonElement;
const greyscaleErosionBtn = document.querySelector('#greyscale-erosion-btn') as HTMLButtonElement;
const greyscaleDilationBtn = document.querySelector('#greyscale-dilation-btn') as HTMLButtonElement;
const greyscaleOpeningBtn = document.querySelector('#greyscale-opening-btn') as HTMLButtonElement;
const greyscaleClosingBtn = document.querySelector('#greyscale-closing-btn') as HTMLButtonElement;

const erosionAnimationBtn = document.querySelector('#erosion-animation-btn') as HTMLButtonElement;
const dilationAnimationBtn = document.querySelector('#dilation-animation-btn') as HTMLButtonElement;
const submitBtn = document.querySelector('#submit-btn') as HTMLButtonElement;
const circleSliders = document.querySelector('#circle-sliders') as HTMLDivElement;
const rectangleSliders = document.querySelector('#rectangle-sliders') as HTMLDivElement;

const leftFormHandler = new FormHandler({
  output: new RadioControl('output', Output.BLACK_AND_WHITE),
  greyscale_formula: new RadioControl('greyscale_formula', GreyscaleFormula.BT_601),
  threshold: new NumberControl('threshold', 50),
  invert: new CheckboxControl('invert', false),
  left_one_color: new TextControl('left_one_color', '#000000'),
});

const rightFormHandler = new FormHandler({
  strel: new RadioControl('strel', StructuringElementType.CIRCLE),
  radius: new NumberControl('radius', 1),
  width: new NumberControl('width', 2),
  height: new NumberControl('height', 2),
  right_one_color: new TextControl('right_one_color', '#000000'),
  delay: new NumberControl('delay', 250),
});

let STATE_L = {zoom: 1, grid: false, numbers: false};
let STATE_R = {zoom: 1, grid: false, numbers: false};

const canvasActionsLeft = new CanvasActions('.canvas-actions-left', STATE_L);
const canvasActionsRight = new CanvasActions('.canvas-actions-right', STATE_R);

let strelType: StructuringElementType = StructuringElementType.CIRCLE;
let RIGHT_CANVAS: MatrixCanvas;
let LEFT_CANVAS: MatrixCanvas;

canvasActionsLeft.changes.subscribe(state => {
  STATE_L = state;
  updateLeftCanvas();
});

canvasActionsRight.changes.subscribe(state => {
  STATE_R = state;
  updateRightCanvas();
});

let LEFT_RGB: RGB[][] = null;
let LEFT_GREYSCALE: number[][] = [[]];
let LEFT_BW: (0 | 1)[][] = [[]];
let RIGHT_BW: (0 | 1)[][] = [[]];
let RIGHT_GRAYSCALE: number[][] =[[]];
let STREL: Strel;
let STREL_2: Strel;
let STREL_MATRIX: (0 | 1)[][] = [[]];

updateStructuringElement();

function updateLeftCanvas() {
  const matrixCanvas = new MatrixCanvas(canvas,  STATE_L.zoom, STATE_L.grid, STATE_L.numbers);
  LEFT_CANVAS = matrixCanvas;
  const output = leftFormHandler.controls.output.getValue();
  const oneColor = leftFormHandler.controls.left_one_color.getValue();
  LEFT_BW = null;
  LEFT_GREYSCALE = null;
  if (output === Output.RGB) {
    matrixCanvas.drawRGB(LEFT_RGB);
  } else if (output === Output.GREYSCALE) {
    LEFT_GREYSCALE = rgbaToGreyscale(LEFT_RGB, leftFormHandler.controls.greyscale_formula.getValue());
    matrixCanvas.drawGreyscale(LEFT_GREYSCALE);
  } else if (output === Output.BLACK_AND_WHITE) {
    const greyscale = rgbaToGreyscale(LEFT_RGB, leftFormHandler.controls.greyscale_formula.getValue());
    let blackAndWhite = greyscaleToBlackAndWhite(greyscale, leftFormHandler.controls.threshold.getValue());
    if (leftFormHandler.controls.invert.getValue()) {
      blackAndWhite = invert(blackAndWhite);
    }
    LEFT_BW = blackAndWhite;
    matrixCanvas.drawMatrix(blackAndWhite, oneColor);
  }
}

makeFileInput('#upload-btn').subscribe(file => {
  imageToMatrix(file)
    .then(m => {
      LEFT_RGB = m;
      updateLeftCanvas();
    });
});

makeFileInput('#strel-upload-btn').subscribe(file => {
  imageToMatrix(file)
    .then(m => {
      const greyscale = rgbaToGreyscale(m);
      const se = greyscaleToBlackWhiteAndRed(greyscale);
      STREL_MATRIX = getValuesFromMatrix(se, 1) as (0 | 1)[][];
      updateStructuringElement();
      const se2 = getValuesFromMatrix(se, -1) as (0 | 1)[][];
      STREL_2 = createStrel(StructuringElementType.CUSTOM, {matrix: se2});
    });
});

leftFormHandler.valueChanges.subscribe(() => {
  if (LEFT_RGB) {
    updateLeftCanvas();
  }
  setTimeout(() => {
    const output = leftFormHandler.controls.output.getValue();
    submitBtn.disabled = output === Output.RGB;
  })
});

rightFormHandler.controls.strel.valueChanges
  .pipe(startWith(strelType))
  .subscribe(strel => {
    strelType = strel;
    onStructuringElementTypeChange();
  });

rightFormHandler.valueChanges.subscribe(() => {
  updateStructuringElement();
});

function updateStructuringElement() {
  const structuringElementType = rightFormHandler.controls.strel.getValue();
  const r = rightFormHandler.controls.radius.getValue();
  const w = rightFormHandler.controls.width.getValue();
  const h = rightFormHandler.controls.height.getValue();
  STREL = createStrel(structuringElementType, {r, h, w, matrix: STREL_MATRIX});
}

rightFormHandler.controls.right_one_color.valueChanges
  .subscribe(() => updateRightCanvas());

function onStructuringElementTypeChange() {
  if (strelType === StructuringElementType.CIRCLE) {
    circleSliders.style.display = 'block';
    rectangleSliders.style.display = 'none';
  } else if (strelType === StructuringElementType.RECTANGLE) {
    circleSliders.style.display = 'none';
    rectangleSliders.style.display = 'block';
  } else if (strelType === StructuringElementType.CUSTOM) {
    rectangleSliders.style.display = 'none';
    circleSliders.style.display = 'none';
  }
}

function updateRightCanvas() {
  const matrixCanvas2 = new MatrixCanvas(canvas2, STATE_R.zoom, STATE_R.grid, STATE_R.numbers);
  if (RIGHT_BW) {
    const oneColor = rightFormHandler.controls.right_one_color.getValue();
    matrixCanvas2.drawMatrix(RIGHT_BW, oneColor);
  } else if (RIGHT_GRAYSCALE) {
    matrixCanvas2.drawGreyscale(RIGHT_GRAYSCALE);
  }
  RIGHT_CANVAS = matrixCanvas2;
}

submitBtn.addEventListener('click', () => {
  if (LEFT_BW) {
    RIGHT_BW = LEFT_BW;
    RIGHT_GRAYSCALE = null;
    updateRightCanvas();
  } else if (LEFT_GREYSCALE) {
    RIGHT_GRAYSCALE = LEFT_GREYSCALE;
    RIGHT_BW = null;
    updateRightCanvas();
  }
});

erosionBtn.addEventListener('click', () => {
  if (RIGHT_BW) {
    RIGHT_BW = erosion(RIGHT_BW, STREL.matrix, STREL.center);
    updateRightCanvas();
  }
});

erosionAnimationBtn.addEventListener('click', () => {
  if (RIGHT_BW) {
    const delay = rightFormHandler.controls.delay.getValue();
    RIGHT_CANVAS.clear(RIGHT_BW.length, RIGHT_BW[0].length);
    erosionAnimation(LEFT_BW, STREL.matrix, STREL.center, RIGHT_CANVAS, LEFT_CANVAS, delay);
  }
});

dilationBtn.addEventListener('click', () => {
  if (RIGHT_BW) {
    RIGHT_BW = dilation(RIGHT_BW, STREL.matrix, STREL.center);
    updateRightCanvas();
  }
});

dilationAnimationBtn.addEventListener('click', () => {
  if (RIGHT_BW) {
    const delay = rightFormHandler.controls.delay.getValue();
    RIGHT_CANVAS.clear(RIGHT_BW.length, RIGHT_BW[0].length);
    dilationAnimation(LEFT_BW, STREL.matrix, STREL.center, RIGHT_CANVAS, LEFT_CANVAS, delay);
  }
});

openingBtn.addEventListener('click', () => {
  if (RIGHT_BW) {
    RIGHT_BW = opening(RIGHT_BW, STREL.matrix, STREL.center);
    updateRightCanvas();
  }
});

closingBtn.addEventListener('click', () => {
  if (RIGHT_BW) {
    RIGHT_BW = closing(RIGHT_BW, STREL.matrix, STREL.center);
    updateRightCanvas();
  }
});

tophatBtn.addEventListener('click', () => {
  if (RIGHT_BW) {
    const OPENING = opening(RIGHT_BW, STREL.matrix, STREL.center);
    RIGHT_BW = diff(RIGHT_BW, OPENING);
    updateRightCanvas();
  }
});

gradientBtn.addEventListener('click', () => {
  if (RIGHT_BW) {
    const DILATION = dilation(RIGHT_BW, STREL.matrix, STREL.center);
    const EROSION = erosion(RIGHT_BW, STREL.matrix, STREL.center);
    RIGHT_BW = diff(DILATION, EROSION);
    updateRightCanvas();
  }
});

diffBtn.addEventListener('click', () => {
  if (LEFT_BW && RIGHT_BW) {
    RIGHT_BW = diff(RIGHT_BW, LEFT_BW);
    updateRightCanvas();
  }
});

rgbDiffBtn.addEventListener('click', () => {
  const output = leftFormHandler.controls.output.getValue();
  if (RIGHT_BW && output === Output.RGB) {
    LEFT_RGB = rgbDiff(LEFT_RGB, RIGHT_BW, STREL);
    updateLeftCanvas();
  }
});

strelBtn.addEventListener('click', () => {
  if (LEFT_BW && RIGHT_BW) {
    RIGHT_BW = STREL.matrix;
    updateRightCanvas();
  }
});

strel2Btn.addEventListener('click', () => {
  if (LEFT_BW && RIGHT_BW) {
    RIGHT_BW = STREL_2.matrix;
    updateRightCanvas();
  }
});

hitOrMissBtn.addEventListener('click', () => {
  if (RIGHT_BW) {
    RIGHT_BW = hitOrMiss(RIGHT_BW, STREL.matrix, STREL.center, STREL_2.matrix, STREL_2.center);
    updateRightCanvas();
  }
});

hitOrMissOpeningBtn.addEventListener('click', () => {
  if (RIGHT_BW) {
    RIGHT_BW = hitOrMiss(RIGHT_BW, STREL.matrix, STREL.center, STREL_2.matrix, STREL_2.center);
    RIGHT_BW = dilation(RIGHT_BW, STREL.matrix, STREL.center);
    updateRightCanvas();
  }
});

greyscaleErosionBtn.addEventListener('click', () => {
  if (RIGHT_GRAYSCALE) {
    // const matrixCanvas2 = new MatrixCanvas(canvas2, STATE_R.zoom, STATE_R.grid, STATE_R.numbers);
    RIGHT_GRAYSCALE = greyscaleErosion(RIGHT_GRAYSCALE, STREL.matrix, STREL.center);
    updateRightCanvas();
    // matrixCanvas2.drawGreyscale(RIGHT_GRAYSCALE);
  }
});

greyscaleDilationBtn.addEventListener('click', () => {
  if (RIGHT_GRAYSCALE) {
    // const matrixCanvas2 = new MatrixCanvas(canvas2, STATE_R.zoom, STATE_R.grid, STATE_R.numbers);
    RIGHT_GRAYSCALE = greyscaleDilation(RIGHT_GRAYSCALE, STREL.matrix, STREL.center);
    updateRightCanvas();
    // matrixCanvas2.drawGreyscale(RIGHT_GRAYSCALE);
  }
});

greyscaleOpeningBtn.addEventListener('click', () => {
  if (RIGHT_GRAYSCALE) {
    RIGHT_GRAYSCALE = greyscaleOpening(RIGHT_GRAYSCALE, STREL.matrix, STREL.center);
    updateRightCanvas();
  }
});

greyscaleClosingBtn.addEventListener('click', () => {
  if (RIGHT_GRAYSCALE) {
    RIGHT_GRAYSCALE = greyscaleClosing(RIGHT_GRAYSCALE, STREL.matrix, STREL.center);
    updateRightCanvas();
  }
});