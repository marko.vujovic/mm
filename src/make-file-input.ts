import { fromEvent, Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';

export const IMAGE_EXTENSIONS = ['.jpg', 'jpeg', '.png', '.bmp', '.svg'];

export function makeFileInput(selector: string, extensions = IMAGE_EXTENSIONS): Observable<File> {
  const trigger = document.querySelector(selector);
  if (!trigger) {
    throw Error(`No element with selector '${selector}'`);
  }
  const fileInput = document.createElement('input');
  fileInput.type = 'file';
  fileInput.accept = extensions.join(',');
  fileInput.classList.add('_hidden');
  document.body.appendChild(fileInput);
  trigger.addEventListener('click', () => fileInput.click());
  return fromEvent(fileInput, 'change')
    .pipe(
      filter(() => fileInput.files.length > 0),
      map(() => fileInput.files[0])
    );
}
