export interface Rectangle {
  width: number;
  height: number;
}

export interface RGB {
  r: number;
  g: number;
  b: number;
}

export enum Output {
  RGB = 'RGB',
  GREYSCALE = 'GREYSCALE',
  BLACK_AND_WHITE = 'BLACK_AND_WHITE'
}

export enum GreyscaleFormula {
  BT_709 = 'BT_709',
  BT_601 = 'BT_601',
  AVERAGE = 'AVERAGE'
}

export enum StructuringElementType {
  CIRCLE = 'CIRCLE',
  RECTANGLE = 'RECTANGLE',
  CUSTOM = 'CUSTOM',
}

export interface Strel {
  matrix: (0 | 1)[][];
  center: {x: number, y: number};
}

export function createStrel(
  type: StructuringElementType,
  config: {r?: number, w?: number, h?: number, matrix?: (0 | 1)[][]},
): Strel {
  if (type === StructuringElementType.RECTANGLE) {
    const {w, h} = config;
    const strel = emptyMatrix(h, w, 1);
    return {
      matrix: strel,
      center: {x: Math.floor((w - 1) / 2), y: Math.floor((h - 1) / 2)}
    }
  } else if (type === StructuringElementType.CIRCLE) {
    const {r} = config;
    const strel = emptyMatrix(r * 2 + 1, r * 2 + 1);
    strel.forEach((row, y) => {
      row.forEach((cell, x) => {
        if (distance({x: r, y: r}, {x: x, y: y}) <= r) {
          row[x] = 1;
        }
      })
    });
    return {
      matrix: strel,
      center: {x: r, y: r}
    }
  } else if (type === StructuringElementType.CUSTOM) {
    const {matrix} = config;
    const h = matrix.length;
    const w = matrix[0].length;
    return {
      matrix: matrix,
      center: {x: Math.floor((w - 1) / 2), y: Math.floor((h - 1) / 2)}
    }
  }


}

function distance(p1: {x: number, y: number}, p2: {x: number, y: number}): number {
  return Math.sqrt(Math.pow(p2.x - p1.x, 2) + Math.pow(p2.y - p1.y, 2));
}

export function greyscaleFormulaWeights(formula: GreyscaleFormula) {
  switch(formula) {
    case GreyscaleFormula.BT_709:
      return {r: 0.21, g: 0.72, b: 0.01};
    case GreyscaleFormula.BT_601:
      return {r: 0.3, g: 0.59, b: 0.11};
    case GreyscaleFormula.AVERAGE:
      return {r: 1/3, g: 1/3, b: 1/3};
  }
}

/**
 * Draws dashed grid on cavas
 * @param {*} canvas canvas to draw grid on
 * @param {*} rows number of grid rows
 * @param {*} cols number of grid columns
 * @param {*} size cell size
 */
export function drawGridOnCanvas(canvas: HTMLCanvasElement, rows: number, cols: number, size: number): void {
  const ctx: CanvasRenderingContext2D = canvas.getContext('2d');
  ctx.strokeStyle = '#2a2a2a';
  ctx.lineWidth = 1;
  for (let i = 0; i <= cols; i++) {
    ctx.beginPath();
    ctx.moveTo(i * size + 0.5, 0);
    ctx.lineTo(i * size + 0.5, rows * size + 0.5);
    ctx.stroke();
  }
  for (let j = 0; j <= rows; j++) {
    ctx.beginPath();
    ctx.moveTo(0 + 0.5, j * size + 0.5);
    ctx.lineTo(cols * size + 0.5, j * size + 0.5);
    ctx.stroke();
  }
}

export function fillCanvasCell(canvas: HTMLCanvasElement, row: number, col: number, size: number, color = 'black') {
  const ctx: CanvasRenderingContext2D = canvas.getContext('2d');
  // smallCanvas.getContext('2d').drawImage(canvas, j * size, i * size, size, size, 0, 0, size, size);
  ctx.fillStyle = color;
  ctx.fillRect(col * size, row * size, size, size);
  ctx.fill();
}

/**
 * Returns dimensions of scaled down image
 * @param {*} image image to scale down
 * @param {*} rectangle 
 */
export function scaleDownImageToFitRectangle(image: HTMLImageElement, rectangle: Rectangle): Rectangle {
  if (image.width > rectangle.width || image.height > rectangle.height) {
    const scalingFactor = Math.min(rectangle.width / image.width, rectangle.height / image.height);
    return {
      width: Math.floor(image.width * scalingFactor),
      height: Math.floor(image.height * scalingFactor)
    }
  }
  return { width: image.width, height: image.height };
}

/**
 * Returns grid cell size which minimizes area which is not going to be covered with grid for the given rectangle
 * @param rectangle
 * @param minSize 
 * @param maxSize 
 */
export function getOptimalGridCellSize(rectangle: Rectangle, minSize = 100, maxSize = 150): number {
  let size = minSize;
  const { width, height } = rectangle;
  let area = Math.floor(width / size) * size * Math.floor(height / size) * size;
  for (let i = size; i <= maxSize; i++) {
    const x = Math.floor(width / i) * i * Math.floor(height / i) * i;
    if (x > area) {
      area = x;
      size = i;
    }
  }
  return size;
}

/**
 * Sets width and height of an element to specified size in pixels
 */
export function setSize(element: HTMLElement, size: number): void {
  element.style.width = element.style.height = size + 'px';
}

/**
 * Clears canvas
 */
export function clearCanvas(canvas: HTMLCanvasElement): void {
  // canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height);
  canvas.getContext('2d').fillStyle = 'white';
  canvas.getContext('2d').fillRect(0, 0, canvas.width, canvas.height);
}

/**
 * Changes order of elements and returns new matrix. The original matrix will not be modified.
 */
export function randomizedMatrix<T>(matrix: T[][]): T[][] {
  const array = [].concat.apply([], matrix) as T[];
  for (let i = array.length - 1; i > 0; i--) {
    const index = Math.floor(Math.random() * (i - 1));
    const temp = array[i];
    array[i] = array[index];
    array[index] = temp;
  }
  const m = [];
  for (let i = 0; i < matrix.length; i++) {
    m.push([]);
    for (let j = 0; j < matrix[i].length; j++) {
      m[i].push(array[i * matrix[i].length + j]);
    }
  }
  return m;
}

export function cloneMatrix<T>(m: T[][]): T[][] {
  const copy = m.slice();
  copy.forEach((row, index) => copy[index] = row.slice());
  return copy;
}

export function emptyMatrix(rows, cols, value = 0): (0 | 1)[][] {
  const e = new Array(rows).fill([]);
  e.forEach((row, index) => e[index] = new Array(cols).fill(value));
  return e;
}

export function imageToMatrix(file: File, maxWidth = 1200, maxHeight = 1200): Promise<RGB[][]> {
  const image = new Image;
  return new Promise((resolve, reject) => {
    image.onload = () => {

      const {width, height} = scaleDownImageToFitRectangle(image, {width: maxWidth, height: maxHeight});

      const canvas = document.createElement('canvas');
      canvas.width = width;
      canvas.height = height;
      canvas.classList.add('_hidden');
      document.body.appendChild(canvas);
      const ctx = canvas.getContext('2d');

      ctx.drawImage(image, 0, 0, image.width, image.height, 0, 0, width, height);
      const imgData = ctx.getImageData(0, 0, width, height).data;
      let map = Array.from(imgData.filter((e, i) => i % 4 === 0))
        .map((r, index) => {
          if (imgData[index * 4 + 3] === 255) { // if alpha is 255
            return {
              r: r,
              g: imgData[index * 4 + 1],
              b: imgData[index * 4 + 2],
            } as any
          } else {
            return { r: 255, g: 255, b: 255 };
          }
        });
      const matrix = arrayToMatrix(map, width);
      resolve(matrix);
    }
    image.src = URL.createObjectURL(file);
  })
}

export function rgbaToGreyscale(matrix: RGB[][], formula = GreyscaleFormula.BT_601): number[][] {
  const weights = greyscaleFormulaWeights(formula);
  return matrix.map(row => {
    return row.map(rgb => {
      return Math.floor(weights.r * rgb.r + weights.g * rgb.g + weights.b * rgb.b);
    })
  })
}

export function greyscaleCrossSections(greyscale: number[][]): number[][][] {
  const crossSections = [];
  const rows = greyscale.length;
  const cols = greyscale[0].length;
  for (let level = 0; level <= 255; level++) {
    const section = emptyMatrix(rows, cols);
    for (let i = 0; i < rows; i++) {
      for (let j = 0; j < cols; j++) {
        if (greyscale[i][j] >= level) {
          section[i][j] = 1;
        }
      }
    }
    crossSections.push(section);
  }
  return crossSections;
}

export function crossSectionsToGreyscale(sections: number[][][]): number[][] {
  const rows = sections[0].length;
  const cols = sections[0][0].length;
  const empty = emptyMatrix(rows, cols) as number[][];
  for (let level = 0; level <= 255; level++) {
    const section = sections[level];
    for (let i = 0; i < rows; i++) {
      for (let j = 0; j < cols; j++) {
        if (section[i][j]) {
          empty[i][j] = level;
        }
      }
    }
  }
  return empty;
}

export function greyscaleToBlackAndWhite(matrix: number[][], threshold = 50): (1 | 0)[][] {
  const min = Math.floor(255 * (threshold / 100));
  return matrix.map(row => {
    return row.map(value => {
      return value > min ? 0 : 1;
    })
  })
}

export function greyscaleToBlackWhiteAndRed(matrix: number[][]): (1 | -1 | 0)[][] {
  return matrix.map(row => {
    return row.map(value => {
      if (value === 0) {
        return 1;
      } else if (value === 255) {
        return 0;
      }
      return -1;
    })
  })
}

export function getValuesFromMatrix(matrix: (1 | -1 | 0)[][], value: 0 | 1 | -1, emptyValue = 0, replaceValue = 1): (1 | 0 | -1)[][] {
  const empty = emptyMatrix(matrix.length, matrix[0].length, emptyValue);
  matrix.forEach((row, rowIndex) => {
    row.forEach((cell, colIndex) => {
      if (cell === value) {
        empty[rowIndex][colIndex] = replaceValue as (1 | 0);
      }
    });
  });
  return empty;
}

export function invert(matrix: (0 | 1)[][]): (0 | 1)[][] {
  const copy = cloneMatrix(matrix);
  copy.forEach(row => {
    row.forEach((cell, index) => row[index] = cell === 1 ? 0 : 1);
  });
  return copy;
}

export function arrayToMatrix<T>(arr: T[], cols: number): T[][] {
  const matrix = [];
  for (let i = 0, row = -1; i < arr.length; i++) {
    if (i % cols === 0) {
      row++;
      matrix.push([]);
    }
    matrix[row].push(arr[i]);
  }
  return matrix;
}
