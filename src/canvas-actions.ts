import {Subject} from "rxjs";


export interface CanvasState {
  zoom: number;
  grid: boolean;
  numbers: boolean;
}

export class CanvasActions {

  private readonly _minZoom: number;
  private readonly _maxZoom: number;
  private _state: CanvasState;
  private _zoomInButton: HTMLButtonElement;
  private _zoomOutButton: HTMLButtonElement;
  private _zoomLevelSelect: HTMLSelectElement;
  private _gridToggle: HTMLButtonElement;
  private _numbersToggle: HTMLButtonElement;
  changes = new Subject<CanvasState>();

  constructor(selector: string, initialState = {zoom: 1, grid: false, numbers: false}, minZoom = 1, maxZoom = 50) {
    const container = document.querySelector(selector);
    this._state = {...initialState};
    container.innerHTML = this._getInnerHTML(minZoom, maxZoom, initialState);
    this._minZoom = minZoom;
    this._maxZoom = maxZoom;
    this._zoomInButton = container.querySelector('.zoom-in') as HTMLButtonElement;
    this._zoomOutButton = container.querySelector('.zoom-out') as HTMLButtonElement;
    this._zoomLevelSelect = container.querySelector('.zoom-level-select') as HTMLSelectElement;
    this._gridToggle = container.querySelector('.grid-toggle') as HTMLButtonElement;
    this._numbersToggle = container.querySelector('.numbers-toggle') as HTMLButtonElement;

    this._zoomInButton.addEventListener('click', () => {
      this._state = {...this._state, zoom: this._state.zoom + 1};
      this._zoomLevelSelect.value = String(this._state.zoom + 1);
      this._update();
    });
    this._zoomOutButton.addEventListener('click', () => {
      this._state = {...this._state, zoom: this._state.zoom - 1};
      this._zoomLevelSelect.value = String(this._state.zoom - 1);
      this._update();
    });
    this._gridToggle.addEventListener('click', () => {
      this._state = {...this._state, grid: !this._state.grid};
      this._update();
    });
    this._numbersToggle.addEventListener('click', () => {
      this._state = {...this._state, numbers: !this._state.numbers};
      this._update();
    });
    this._zoomLevelSelect.addEventListener('change', event => {
      console.log(event);
      this._state = {...this._state, zoom: +this._zoomLevelSelect.value};
      this._update();
    });
    this._update();
  }

  private _update() {
    this._zoomOutButton.disabled = this._state.zoom === this._minZoom;
    this._zoomInButton.disabled = this._state.zoom === this._maxZoom;
    this._zoomLevelSelect.value = String(this._state.zoom);
    // this._gridToggle.disabled = this._state.zoom < 5;
    if (this._state.grid) {
      this._gridToggle.classList.add('on');
    }
    this.changes.next(this._state);
  }

  private _getInnerHTML(minZoom: number, maxZoom: number, initialState = {zoom: 1, grid: false}): string {
    let select = '<select class="zoom-level-select">';
    for (let i = minZoom; i <= maxZoom; i++) {
      select += `<option>${i}</option>`;
    }
    select += '</select>';
    return `
       <span class="ui vertical buttons">
         <button class="ui icon button mini zoom-in">
              <i class="icon zoom-in"></i>
         </button>
         
         <button class="ui icon button mini zoom-out">
              <i class="icon zoom-out"></i>
         </button>
         
         <button class="ui icon button mini grid-toggle">
              <i class="icon th"></i>
         </button>
         
         <button class="ui icon button mini numbers-toggle">
              01
         </button>
       </span>
       
       ${select}
    `;
  }

}
