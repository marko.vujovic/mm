import { Subject } from "rxjs";

export abstract class AbstractControl {
  valueChanges = new Subject<any>();
  abstract getValue(): any;
  abstract setValue(value: any): void;
}

export class RadioControl extends AbstractControl {

  private _inputs: HTMLInputElement[];

  constructor(name: string, initialValue?: string) {
    super();
    this._inputs = <HTMLInputElement[]> Array.from(document.querySelectorAll(`input[name="${name}"]`));
    this._inputs.forEach(input => {
      input.addEventListener('change', () => this.valueChanges.next(this.getValue()));
    });
    if (initialValue !== null && initialValue !== undefined) {
      this.setValue(initialValue);
    }
  }

  getValue(): string {
    const checked = this._getChecked();
    return checked ? checked.value : null;
  }

  setValue(value: string) {
    const toCheck = this._inputs.find(input => input.value === value);
    if (toCheck) {
      toCheck.checked = true;
      this.valueChanges.next(value);
    } else {
      throw new Error(`No option with value ${value}`);
    }
  }

  private _getChecked(): HTMLInputElement {
    return this._inputs.find(input => input.checked);
  }

}

export class CheckboxControl extends AbstractControl {

  private readonly _input: HTMLInputElement;

  constructor(name: string, initialValue?) {
    super();
    this._input = <HTMLInputElement> document.querySelector(`input[name="${name}"]`);
    if (!this._input) {
      throw new Error(`No input element with name = ${name}`)
    }
    this._input.addEventListener('change', () => {
      this.valueChanges.next(this.getValue());
    });
    if (initialValue !== null && initialValue !== undefined) {
      this.setValue(initialValue);
    }
  }

  getValue(): any {
    // console.log(this._input.checked);
    return this._input.checked
  }

  setValue(value: any) {
    this._input.checked = value;
    this.valueChanges.next(value);
  }

}

export class TextControl extends AbstractControl {

  private readonly _input: HTMLInputElement;

  constructor(name: string, initialValue?) {
    super();
    this._input = <HTMLInputElement> document.querySelector(`input[name="${name}"]`);
    if (!this._input) {
      throw new Error(`No input element with name = ${name}`)
    }
    this._input.addEventListener('input', () => {
      this.valueChanges.next(this.getValue());
    });
    if (initialValue !== null && initialValue !== undefined) {
      this.setValue(initialValue);
    }
  }

  getValue(): any {
    return this._input.value;
  }

  setValue(value: any) {
    this._input.value = value;
    this.valueChanges.next(value);
  }

}

export class NumberControl extends AbstractControl {

  private readonly _input: HTMLInputElement;

  constructor(name: string, initialValue: number) {
    super();
    this._input = <HTMLInputElement> document.querySelector(`input[name="${name}"]`);
    if (!this._input) {
      throw new Error(`No input element with name = ${name}`)
    }
    this._input.addEventListener('input', () => {
      this.valueChanges.next(this.getValue());
    });
    if (initialValue !== null && initialValue !== undefined) {
      this.setValue(+initialValue);
    }
  }

  getValue(): number {
    return +this._input.value;
  }

  setValue(value: number | string) {
    this._input.value = <string> value;
    this.valueChanges.next(+value);
  }

}



export class FormHandler<T> {

  controls: {[key: string]: AbstractControl} = {};
  valueChanges = new Subject();

  constructor(config: {[key: string]: AbstractControl}) {
    this.controls = config;
    Object.entries(this.controls).forEach(([key, control]) => {
      control.valueChanges.subscribe(() => this.valueChanges.next(this.getValue()));
    });
  }

  setValue(value: {[key: string]: any}) {
    Object.entries(value).forEach(([key, value]) => {
      if (this.controls[key]) {
        this.controls[key].setValue(value);
      }
    });
  }

  getValue(): any {
    const value = {};
    Object.entries(this.controls).forEach(([key, control]) => {
      value[key] = control.getValue();
    });
    return value;
  }

}