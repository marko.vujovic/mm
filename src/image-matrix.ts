import {RGB, clearCanvas, drawGridOnCanvas} from "./functions";

export class MatrixCanvas {

  _canvas: HTMLCanvasElement;
  _ctx: CanvasRenderingContext2D;
  _size: number;
  _showGrid: boolean;
  _showNumbers: boolean;
  // total width and height
  _width: number;
  _height: number;
  //

  constructor(canvas: HTMLCanvasElement, size = 4, showGrid = false, showNumbers = false) {
    this._canvas = canvas;
    this._ctx = this._canvas.getContext('2d');
    this._showGrid = showGrid;
    this._showNumbers = showNumbers;
    this._size = size;
  }

  clear(rows: number, columns: number) {
    clearCanvas(this._canvas);
    if (this._showGrid) {
      this.drawGrid(rows, columns);
    }
  }

  fillCell(x: number, y: number, color: string) {
    this._ctx.fillStyle = color;
    const size = this._showGrid ? this._size - 1: this._size;
    const add = this._showGrid ? 1 : 0;
    this._ctx.fillRect(x * this._size + add, y * this._size + add, size, size);
  }

  drawMark(x: number, y: number, color: string) {
    this._ctx.fillStyle = color;
    const size = Math.ceil(this._size / 5);
    this._ctx.fillRect(x * this._size + size * 2, y * this._size + size * 2, size, size);
  }

  drawGrid(rows: number, columns: number) {
    drawGridOnCanvas(this._canvas, rows, columns, this._size);
  }

  getContext(): CanvasRenderingContext2D {
    return this._ctx;
  }

  drawMatrix(matrix: (0 | 1)[][], color = 'black') {
    this._setCanvasDimensions(matrix.length, matrix[0].length);
    this._ctx.fillStyle = color;
    for (let y = 0; y < matrix.length; y++) {
      const row = matrix[y];
      for (let x = 0; x < row.length; x++) {
        if (row[x]) {
          this._ctx.fillStyle = color;
          this._ctx.fillRect(x * this._size, y * this._size, this._size, this._size);
          if (this._showNumbers) {
            this._ctx.textBaseline = 'middle';
            this._ctx.textAlign = "center";
            this._ctx.fillStyle = 'white';
            const fontArgs = this._ctx.font.split(' ');
            const newSize = `${Math.floor(this._size / 2)}px`;
            this._ctx.font = newSize + ' ' + fontArgs[fontArgs.length - 1]; /// using the last part
            this._ctx.fillText('1', x * this._size + this._size / 2, y * this._size + this._size / 2, this._size);
          }
        } else if (this._showNumbers) {
          this._ctx.textBaseline = 'middle';
          this._ctx.textAlign = "center";
          this._ctx.fillStyle = color;
          const fontArgs = this._ctx.font.split(' ');
          const newSize = `${Math.floor(this._size / 2)}px`;
          this._ctx.font = newSize + ' ' + fontArgs[fontArgs.length - 1]; /// using the last part
          this._ctx.fillText('0', x * this._size + this._size / 2, y * this._size + this._size / 2, this._size);
        }
      }
    }
    if (this._showGrid) {
      this.drawGrid(matrix.length, matrix[0].length);
    }
  }

  drawGreyscale(matrix: number[][]) {
    this._setCanvasDimensions(matrix.length, matrix[0].length);
    for (let y = 0; y < matrix.length; y++) {
      const row = matrix[y];
      for (let x = 0; x < row.length; x++) {
        const cell = row[x];
        this._ctx.fillStyle = `rgb(${cell}, ${cell}, ${cell})`;
        this._ctx.fillRect(x * this._size, y * this._size, this._size, this._size);
      }
    }
    if (this._showGrid) {
      drawGridOnCanvas(this._canvas, matrix.length, matrix[0].length, this._size);
    }
  }

  drawRGB(matrix: RGB[][]) {
    this._setCanvasDimensions(matrix.length, matrix[0].length);
    for (let y = 0; y < matrix.length; y++) {
      const row = matrix[y];
      for (let x = 0; x < row.length; x++) {
        const cell = row[x];
        this._ctx.fillStyle = `rgb(${cell.r}, ${cell.g}, ${cell.b})`;
        this._ctx.fillRect(x * this._size, y * this._size, this._size, this._size);
      }
    }
    if (this._showGrid) {
      drawGridOnCanvas(this._canvas, matrix.length, matrix[0].length, this._size);
    }
  }

  private _setCanvasDimensions(h: number, w: number) {
    this._canvas.width = w * this._size + (this._showGrid ? 1 : 0);
    this._canvas.height = h * this._size + (this._showGrid ? 1 : 0);
  }

}
