import {cloneMatrix, emptyMatrix, invert, RGB, Strel} from "./functions";
import {MatrixCanvas} from "./image-matrix";

function fits(a: number[][], b: number[][], aX: number, aY: number, center: { x: number, y: number }) {
  const v = { x: -center.x, y: -center.y };
  for (let y0 = 0; y0 < b.length; y0++) {
    const row = b[y0];
    for (let x0 = 0; x0 < row.length; x0++) {
      const cell = row[x0];
      const y = y0 + v.y;
      const x = x0 + v.x;
      if (cell) {
        const xx = aX + x;
        const yy = aY + y;
        if (xx >= 0 && yy >= 0 && yy < a.length && xx < a[yy].length) { // check if inside a
          if (!a[yy][xx]) {
            return false;
          }
        } else {
          return false;
        }
      }
    }
  }
  return true;
}

export function erosion(a: (0 | 1)[][], b: (0 | 1)[][], bCenter: {x: number, y: number}): (0 | 1)[][] {
  const empty = emptyMatrix(a.length, a[0].length);
  a.forEach((row, y) => {
    row.forEach((cell, x) => {
      if (fits(a, b, x, y, bCenter)) {
        empty[y][x] = 1;
      }
    });
  });
  return empty;
}


export function erosionAnimation(
  a: (0 | 1)[][],
  b: (0 | 1)[][],
  bCenter: {x: number, y: number},
  rightCanvas: MatrixCanvas,
  leftCanvas: MatrixCanvas,
  delay: number,
) {
  const delay2 = Math.floor(delay - 10);
  a.forEach((row, y) => {
    row.forEach((cell, x) => {
      if (fits(a, b, x, y, bCenter)) {
        // draw blue

        const v = { x: -bCenter.x, y: -bCenter.y };
        setTimeout(() => {
          for (let y0 = 0; y0 < b.length; y0++) {
            const row1 = b[y0];
            for (let x0 = 0; x0 < row1.length; x0++) {
              const cel1l = row1[x0];
              if (cel1l) {
                const y1 = y0 + v.y;
                const x1 = x0 + v.x;
                const xx = x + x1;
                const yy = y + y1;
                // draw blue a[yy][xx]
                leftCanvas.fillCell(xx, yy, 'rgba(25,25,200,0.5)');
                if (x0 === bCenter.x && y0 === bCenter.y) {
                  leftCanvas.drawMark(xx,yy,'rgba(255,255,255,0.75)');
                }
              }
            }
          }
          requestAnimationFrame(() => {});
          setTimeout(() => {
            for (let y0 = 0; y0 < b.length; y0++) {
              const row1 = b[y0];
              for (let x0 = 0; x0 < row1.length; x0++) {
                const cel1l = row1[x0];
                if (cel1l) {
                  const y1 = y0 + v.y;
                  const x1 = x0 + v.x;
                  const xx = x + x1;
                  const yy = y + y1;
                  // draw blue a[yy][xx]
                  leftCanvas.fillCell(xx, yy, (a[yy] || [])[xx] ? 'black' : 'white');
                }
              }
            }
            requestAnimationFrame(() => {});
            // a[y][x] = 1;
          }, delay2);
          rightCanvas.fillCell(x, y, 'black');
        }, delay * (y * a[0].length + x));
      } else {
      // draw red
        const v = { x: -bCenter.x, y: -bCenter.y };
        setTimeout(() => {
          for (let y0 = 0; y0 < b.length; y0++) {
            const row1 = b[y0];
            for (let x0 = 0; x0 < row1.length; x0++) {
              const cel1l = row1[x0];
              if (cel1l) {
                const y1 = y0 + v.y;
                const x1 = x0 + v.x;
                const xx = x + x1;
                const yy = y + y1;
                // draw blue a[yy][xx]
                leftCanvas.fillCell(xx, yy, 'rgba(200,0,0,0.5)');
                if (x0 === bCenter.x && y0 === bCenter.y) {
                  leftCanvas.drawMark(xx,yy,'rgba(255,255,255,0.75)');
                }
              }
            }
            requestAnimationFrame(() => {});
          }
          setTimeout(() => {
            for (let y0 = 0; y0 < b.length; y0++) {
              const row1 = b[y0];
              for (let x0 = 0; x0 < row1.length; x0++) {
                const cel1l = row1[x0];
                if (cel1l) {
                  const y1 = y0 + v.y;
                  const x1 = x0 + v.x;
                  const xx = x + x1;
                  const yy = y + y1;
                  // draw blue a[yy][xx]
                  leftCanvas.fillCell(xx, yy, (a[yy] || [])[xx] ? 'black' : 'white');
                }
              }
              requestAnimationFrame(() => {});
            }
            // rightCanvas.fillCell(x, y, 'white');
            // a[y][x] = 0;
          }, delay2);
        }, delay * (y * a[0].length + x));
      }
    });
  });
}

function expand(a: (0 | 1)[][], b: (0 | 1)[][], aX, aY, center: {x: number, y: number}) {
  const v = { x: -center.x, y: -center.y };
  for (let y0 = 0; y0 < b.length; y0++) {
    const row = b[y0];
    for (let x0 = 0; x0 < row.length; x0++) {
      const cell = row[x0];
      const y = y0 + v.y;
      const x = x0 + v.x;
      if (cell) {
        const xx = aX + x;
        const yy = aY + y;
        if (xx >= 0 && yy >= 0 && yy < a.length && xx < a[yy].length) { // check if inside a
          a[yy][xx] = 1;
        }
      }
    }
  }
}

export function dilation(a: (0 | 1)[][], b: (0 | 1)[][], bCenter: {x: number, y: number}): (0 | 1)[][] {
  const empty = emptyMatrix(a.length, a[0].length);
  a.forEach((row, y) => {
    row.forEach((cell, x) => {
      if (cell) {
        expand(empty, b, x, y, bCenter);
      }
    });
  });
  return empty;
}

export function dilationAnimation(
  a: (0 | 1)[][],
  b: (0 | 1)[][],
  bCenter: {x: number, y: number},
  rightCanvas: MatrixCanvas,
  leftCanvas: MatrixCanvas,
  delay: number,
) {
  const delay2 = Math.floor(delay - delay * 0.1);
  a.forEach((row, y) => {
    row.forEach((cell, x) => {
      if (cell) {
        // draw blue

        const v = { x: -bCenter.x, y: -bCenter.y };
        setTimeout(() => {
          for (let y0 = 0; y0 < b.length; y0++) {
            const row1 = b[y0];
            for (let x0 = 0; x0 < row1.length; x0++) {
              const cel1l = row1[x0];
              if (cel1l) {
                const y1 = y0 + v.y;
                const x1 = x0 + v.x;
                const xx = x + x1;
                const yy = y + y1;
                // draw blue a[yy][xx]
                leftCanvas.fillCell(xx, yy, 'rgba(0,0,200,0.5)');
                if (x0 === bCenter.x && y0 === bCenter.y) {
                  leftCanvas.drawMark(xx,yy,'rgba(255,255,255,0.75)');
                }
              }
            }
          }
          requestAnimationFrame(() => {});
          setTimeout(() => {
            for (let y0 = 0; y0 < b.length; y0++) {
              const row1 = b[y0];
              for (let x0 = 0; x0 < row1.length; x0++) {
                const cel1l = row1[x0];
                if (cel1l) {
                  const y1 = y0 + v.y;
                  const x1 = x0 + v.x;
                  const xx = x + x1;
                  const yy = y + y1;
                  // draw blue a[yy][xx]
                  leftCanvas.fillCell(xx, yy, (a[yy] || [])[xx] ? 'black' : 'white');
                }
              }
            }
            requestAnimationFrame(() => {});
            // replace this with
            // rightCanvas.fillCell(x, y, 'black');
            // this
            for (let y0 = 0; y0 < b.length; y0++) {
              const row1 = b[y0];
              for (let x0 = 0; x0 < row1.length; x0++) {
                const cel1l = row1[x0];
                if (cel1l) {
                  const y1 = y0 + v.y;
                  const x1 = x0 + v.x;
                  const xx = x + x1;
                  const yy = y + y1;
                  // draw blue a[yy][xx]
                  rightCanvas.fillCell(xx, yy, 'black');
                }
              }
            }

            // a[y][x] = 1;
          }, delay2);
        }, delay * (y * a[0].length + x));
      } else {
        // draw red
        const v = { x: -bCenter.x, y: -bCenter.y };
        setTimeout(() => {
          for (let y0 = 0; y0 < b.length; y0++) {
            const row1 = b[y0];
            for (let x0 = 0; x0 < row1.length; x0++) {
              const cel1l = row1[x0];
              if (cel1l) {
                const y1 = y0 + v.y;
                const x1 = x0 + v.x;
                const xx = x + x1;
                const yy = y + y1;
                // draw blue a[yy][xx]
                leftCanvas.fillCell(xx, yy, 'rgba(200,0,0,0.5)');
                if (x0 === bCenter.x && y0 === bCenter.y) {
                  leftCanvas.drawMark(xx,yy,'rgba(255,255,255,0.75)');
                }
              }
            }
            requestAnimationFrame(() => {});
          }
          setTimeout(() => {
            for (let y0 = 0; y0 < b.length; y0++) {
              const row1 = b[y0];
              for (let x0 = 0; x0 < row1.length; x0++) {
                const cel1l = row1[x0];
                if (cel1l) {
                  const y1 = y0 + v.y;
                  const x1 = x0 + v.x;
                  const xx = x + x1;
                  const yy = y + y1;
                  // draw blue a[yy][xx]
                  leftCanvas.fillCell(xx, yy, (a[yy] || [])[xx] ? 'black' : 'white');
                }
              }
              requestAnimationFrame(() => {});
            }
            // rightCanvas.fillCell(x, y, 'white');
            // a[y][x] = 0;
          }, delay2);
        }, delay * (y * a[0].length + x));
      }
    });
  });
}

export function opening(a: (0 | 1)[][], b: (0 | 1)[][], bCenter: {x: number, y: number}): (0 | 1)[][]  {
  const eroded = erosion(a, b, bCenter);
  return dilation(eroded, b, bCenter);
}

export function closing(a: (0 | 1)[][], b: (0 | 1)[][], bCenter: {x: number, y: number}): (0 | 1)[][]  {
  const dilated = dilation(a, b, bCenter);
  return erosion(dilated, b, bCenter);
}

export function diff(a: (0 | 1)[][], b: (0 | 1)[][]): (0 | 1)[][] {
  const d = cloneMatrix(a);
  d.forEach((row, y) => {
    row.forEach((cell, x) => {
      d[y][x] = <any> Math.abs(a[y][x] - b[y][x]);
    });
  });  return d;
}

export function rgbDiff(a: RGB[][], b: (0 | 1)[][], strel: Strel): (RGB | null)[][] {
  const d = cloneMatrix(a);
  b.forEach((row, y) => {
    row.forEach((cell, x) => {
      if(cell) {
        d[y][x] = null;
      }
    })
  });
  d.forEach((row, y) => {
    row.forEach((cell, x) => {
      if (!cell) {
        const N = neighbourhood(d, strel.matrix, strel.center, x, y);
        d[y][x] = averageColor(N as RGB[]);
      }
    });
  });
  return d;
}

export function neighbourhood(a: (RGB | null | number)[][], b: (0 | 1)[][], bCenter: {x: number, y: number}, x: number, y: number): (RGB | number)[] {
  const N: (RGB | number)[] = [];
  b.forEach((row, yy) => {
    row.forEach((cell, xx) => {
      if(cell) {
        const v = { x: -bCenter.x, y: -bCenter.y };
        const aX = x + xx + v.x;
        const aY = y + yy + v.y;
        const pixel = a[aY] ? a[aY][aX] : null;
        if (pixel !== null && pixel !== undefined) {
          N.push(pixel);
        }
      }
    });
  });
  return N;
}

export function averageColor(pixels: (RGB)[]): RGB {
  const sum: RGB = {r: 0, g: 0, b: 0};
  pixels.forEach(pixel => {
    sum.r += pixel.r;
    sum.g += pixel.g;
    sum.b += pixel.b;
  });
  return {
    r: Math.floor(sum.r / pixels.length),
    g: Math.floor(sum.g / pixels.length),
    b: Math.floor(sum.b / pixels.length),
  }
}

export function hitOrMiss(
  a: (0 | 1)[][],
  b1: (0 | 1)[][],
  b1Center: {x: number, y: number},
  b2: (0 | 1)[][],
  b2Center: {x: number, y: number},
): (0 | 1)[][] {
  const empty = emptyMatrix(a.length, a[0].length);
  const inverse = invert(a);
  a.forEach((row, y) => {
    row.forEach((cell, x) => {
      if (fits(a, b1, x, y, b1Center) && fits(inverse, b2, x, y, b2Center)) {
        empty[y][x] = 1;
      }
    });
  });
  return empty;
}


export function greyscaleErosion(greyscale: number[][], b: (0 | 1)[][], bCenter: {x: number, y: number}): number[][] {
  const empty = emptyMatrix(greyscale.length, greyscale[0].length) as number[][];
  greyscale.forEach((row, y) => {
    row.forEach((cell, x) => {
      const N = neighbourhood(greyscale, b, bCenter, x, y) as number[];
      empty[y][x] = Math.min(...N);
    });
  });
  return empty;
}

export function greyscaleDilation(greyscale: number[][], b: (0 | 1)[][], bCenter: {x: number, y: number}): number[][] {
  const empty = emptyMatrix(greyscale.length, greyscale[0].length) as number[][];
  greyscale.forEach((row, y) => {
    row.forEach((cell, x) => {
      const N = neighbourhood(greyscale, b, bCenter, x, y) as number[];
      empty[y][x] = Math.max(...N);
    });
  });
  return empty;
}

export function greyscaleOpening(greyscale: number[][], b: (0 | 1)[][], bCenter: {x: number, y: number}): number[][] {
  const eroded = greyscaleErosion(greyscale, b, bCenter);
  return greyscaleDilation(eroded, b, bCenter);
}

export function greyscaleClosing(greyscale: number[][], b: (0 | 1)[][], bCenter: {x: number, y: number}): number[][] {
  const dilated = greyscaleDilation(greyscale, b, bCenter);
  return greyscaleErosion(dilated, b, bCenter);
}